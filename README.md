# QSAR: predicting SARS-CoV-2 ADP-ribose phosphatase binding activity

Global AI Challenge 2022

Solution by the Semyons team

## Requirements
- Linux (for shell scripts in `voter_svm`). 
- R
- Conda

## Setup

```shell
git clone https://gitlab.com/semyons/qsar.git
cd qsar
conda create -n qsar python=3.9
conda activate qsar
conda install -c conda-forge pytorch dglteam --file requirements.txt
pip install tensorflow-gpu~=2.4 # for machines without GPU: pip install tensorflow~=2.4
```

If you use PyCharm, it is convenient to register the newly created Conda environment as a project interpreter: `Add interpreter -> Conda environment -> Existing environment -> <path-to-Conda>/envs/qsar/python.exe`

## Running scripts

Execute scripts from `src` one-by-one in the following order:
- `train_valid_split.ipynb` (you may skip this step: output(`data/train_extended.csv`) is already in the VCS)
- `voter_svm`:
   - `train.sh`
   - `evaluate.sh`
- `voter_sum`:
   - `pre_anal.Rmd` (you may skip this step: outputs (`data/trainAnnaFeatures.csv`, `data/testAnnaFeatures.csv`) are already in the VCS)
   - `train_nnets.ipynb`
   - `pool.ipynb`
- `voter_catboost`:
   - **TODO** feature generator. For now, already generated features are available (`data/features_anna2.txt`)
   - `chem_features_converter.ipynb`
   - `train_nnets.ipynb`
   - `filtering.ipynb`
- `random_forest`
   - **TODO**
- `main.ipynb` (well, it works for now even without `random_forest`, but it produces not the same results as we used in the final submission)

**We're still assembling the project**

Should you have any questions, [open an issue](https://gitlab.com/semyons/qsar_public/-/issues) or write to the capitan:

[valera.zuev.zva@gmail.com](mailto:valera.zuev.zva@gmail.com) | [t.me/zuevval](https://t.me/zuevval)
