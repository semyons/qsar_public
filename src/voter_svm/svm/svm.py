from sklearnex import patch_sklearn
patch_sklearn()

from grakel import GraphKernel
import pandas as pd
from rdkit import Chem
import os
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, cross_val_predict
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
from grakel.utils import cross_validate_Kfold_SVM
import numpy as np


seed = 2022
data_folder = 'Data'
filename_test = os.path.join(data_folder, 'test.csv')
filename_train = os.path.join(data_folder, 'train.csv')

def smiles_to_graph(smiles):
    mol = Chem.MolFromSmiles(smiles)
    inds = {i: atom.GetSymbol() for i, atom in enumerate(mol.GetAtoms())}
    adj = Chem.GetAdjacencyMatrix(mol, useBO=True).tolist()
    return [adj, inds]

def read_data(filename: str) -> pd.DataFrame:
    sep = '\t' if filename.endswith('.tsv') else ','
    df = pd.read_csv(filename, sep=sep, index_col=0)
    if 'Pubs' in df.columns:
        return df.drop('Pubs', axis=1)
    return df



df_test = read_data(filename_test)
df_train = read_data(filename_train)

x_test = list(map(smiles_to_graph, df_test.Smiles))
x_train = list(map(smiles_to_graph, df_train.Smiles))
y_train = df_train['Active'].values


kernels = list()
kernels_params = list()

ker = [{'name': 'weisfeiler_lehman', 'n_iter': 3},
       {'name': 'vertex_histogram'}]
x = x_train + x_test
k = GraphKernel(ker, normalize=True, n_jobs=-1, random_state=seed).fit(x_train)
gk = k.transform(x_train)
gt = k.transform(x_test)
svc = SVC(C=1, class_weight='balanced').fit(gk, y_train)
# train_k = k.transform(x_train)

# # from grakel.kernels import Propagation
# # k = Propagation(normalize=True)
# # kernels.append(k.fit_transform(x_train))
# for i in range(1, 8):
#     k = GraphKernel([{'name': 'weisfeiler_lehman', 'n_iter': i}],
#                     normalize='True')
#     kernels.append(k.fit_transform(x_train))

# print('Training...')
# accs = cross_validate_Kfold_SVM(kernels, y_train, n_iter=10, n_splits=5,
#                                 scoring='f1', random_state=seed)
# for i in range(1, 2):
#     kernels_params.append([{'name': 'weisfeiler_lehman', 'n_iter': i},
#                             {'name': 'vertex_histogram'}])
# for i in range(1, 2):
#     kernels_params.append([{'name': 'weisfeiler_lehman', 'n_iter': i},
#                             {'name': 'shortest_path', 'with_labels': True}])
# # for i in range(1, 15):
# #     kernels_params.append([{'name': 'weisfeiler_lehman', 'n_iter': i},
# #                            {'name': 'NSPD'}])

# for p in kernels_params:
#     print('Computing:', p)
#     kernels.append(GraphKernel(p, normalize=True, random_state=seed,
#                                n_jobs=10).fit_transform(x_train))

# print('Training...')
# accs = cross_validate_Kfold_SVM(kernels, y_train, n_iter=10, n_splits=5,
#                                 scoring='f1', random_state=seed)
# best_score = -float('inf')
# best_res = None
# for acc, p in zip(accs, kernels_params):
#     mean = np.mean(acc)
#     std = np.std(acc)
#     print('{:.3f} +- {:.3f}'.format(mean, std), p)
#     if mean > best_score:
#         best_score = mean
#         best_res = p

# k = GraphKernel(best_res, normalize=True, random_state=seed, n_jobs=10).fit(x_train)
# gk = k.transform(x_train)
# gk_test = k.transform(x_test)
# svc = SVC(kernel='precomputed', class_weight='balanced')
# # gk = GraphKernel(kernel=kernel, normalize=True)
# grid_svc = {'C': np.linspace(1, 40, 10)}
# # gk = gk.fit_transform(x_train)
# estimator = make_pipeline(
#     GraphKernel(kernel=kernel, normalize=True),
#     GridSearchCV(SVC(kernel='precomputed'), dict(C=C_grid),
#                  scoring='f1', cv=5))