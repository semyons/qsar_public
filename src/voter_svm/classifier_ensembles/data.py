"""Dataset loader."""
import os
import pandas as pd
import numpy as np
import torch
import dgl
import dill

from dgllife.utils import get_mol_3d_coordinates,\
    smiles_to_nearest_neighbor_graph, smiles_to_bigraph,\
    CanonicalAtomFeaturizer, CanonicalBondFeaturizer, WeaveAtomFeaturizer,\
    WeaveEdgeFeaturizer,\
    BaseAtomFeaturizer, BaseBondFeaturizer, atom_type_one_hot, bond_type_one_hot,\
    atomic_number
from tqdm import tqdm
from torch.utils.data import Dataset
from rdkit import Chem
from rdkit.Chem import AllChem


class MoleculeDataset(Dataset):
    def __init__(self,
                 filename='Data/train.csv',
                 cutoff=5.0,
                 cache_dir='Data',
                 features='base',
                 force_reload=False,
                 triplets=True):
        super().__init__()
        self.feat = features
        self.cutoff = cutoff
        self.load(filename, force_reload, cache_dir)
        self.triplets = triplets
        self.process()

    def load(self, filename: str, force_reload: bool, cache_dir: str):
        f = os.path.join(cache_dir,
                         '.'.join(filename.split('/')[-1].split('.')[:-1]) + '.pkl')
        if force_reload or not os.path.isfile(f):
            sep = ',' if filename.endswith('.csv') else '\t'
            df = pd.read_csv(filename, sep=sep, index_col=0)
            self.labels = list()
            graphs = list()
            line_graphs = list()
            errors_smiles = list()
            errors_label = list()
            base_feat = self.feat
            atoms = ['C', 'N', 'O', 'F', 'P', 'Cl', 'Br', 'Na', 'As', 'I',
                     'B', 'K', 'Zn', 'H']
            if base_feat == 'base':
                atom_featurizer = BaseAtomFeaturizer({'h':atom_type_one_hot})
            elif base_feat == 'mcgn':
                atom_featurizer = BaseAtomFeaturizer({'h': atomic_number})
            elif base_feat == 'cannon':
                # atom_featurizer = CanonicalAtomFeaturizer()
                atom_featurizer = WeaveAtomFeaturizer(atom_types=atoms)
            if base_feat == 'cannon':
                bond_featurizer = CanonicalBondFeaturizer()
                # bond_featurizer = WeaveEdgeFeaturizer()
            else:
                bond_featurizer = BaseBondFeaturizer({'e': bond_type_one_hot})
            for ind, row in tqdm(list(df.iterrows())):
                sm = row.Smiles
                if self.cutoff is not None:
                    mol = Chem.MolFromSmiles(sm)
                    emb = AllChem.EmbedMolecule(mol, maxAttempts=10)
                    if emb == -1:
                        emb = AllChem.EmbedMolecule(mol, maxAttempts=100,
                                                    useRandomCoords=True,
                                                    randomSeed=2021,
                                                    enforceChirality=False,
                                                    ignoreSmoothingFailures=True,
                                                    numZeroFail=3)
                    if emb == -1:
                        errors_smiles.append(sm)
                        if self.labels is not None and 'Active' in 'row':
                            errors_label.append(row.Active)
                        continue
                    AllChem.MMFFOptimizeMolecule(mol, maxIters=1000)
                    coords = get_mol_3d_coordinates(mol)
                    g = smiles_to_nearest_neighbor_graph(sm, coords, self.cutoff,
                                                         keep_dists=True,
                                                         node_featurizer=atom_featurizer)
                else:
                    g = smiles_to_bigraph(sm,
                                          node_featurizer=atom_featurizer,
                                          edge_featurizer=bond_featurizer)
                if not g.num_edges() or not g.num_nodes():
                    errors_smiles.append(sm)
                    continue
                if 'Active' in row:
                    self.labels.append(row.Active)
                g = dgl.add_self_loop(g)
                lg = dgl.line_graph(g, backtracking=False)
                g.mol_id = ind
                graphs.append(g)
                line_graphs.append(lg)
            if not self.labels:
                self.labels = None
            else:
                self.labels = np.array(self.labels)
            self.graphs = graphs
            self.line_graphs = line_graphs
            self._error_smiles = errors_smiles
            self._error_labels = errors_label
            with open(f, 'wb') as f:
                dill.dump((graphs, line_graphs, self.labels), f)
        else:
            with open(f, 'rb') as f:
                self.graphs, self.line_graphs, self.labels = dill.load(f)

    def process(self):
        self.inds_a = np.where(self.labels == False)[0]
        self.inds_b = np.where(self.labels == True)[0]
        
        
        

    def __getitem__(self, idx):
        return self.graphs[idx], self.labels[idx]


    def __len__(self):
        return len(self.graphs)

    def get_collate(self):
        return _collate_fn

def _collate_fn(batch):
    if len(batch[0]) == 2:
        graphs, labels = map(list, zip(*batch))
        labels = torch.tensor(labels, dtype=float).reshape(-1, 1)
        return dgl.batch(graphs), labels
    return dgl.batch(graphs(batch))
