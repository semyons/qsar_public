import os
import pandas as pd
import numpy as np
from sys import argv

folder = 'Results'
df_test = 'Data/test.csv'

filenames = list(filter(lambda x: x.endswith('.csv'), os.listdir(folder)))
filenames = list(map(lambda x: os.path.join(folder, x), filenames))



df = pd.read_csv(df_test, index_col=0)
scores = list()
for filename in filenames:
    dft = pd.read_csv(filename, index_col=0)
    try:
        n = int(filename.split('/')[-1].split('.csv')[0])
    except ValueError:
        continue
    df[f'score{n}'] = dft.score
    scores.append(f'score{n}')
df['score'] = df[scores].mean(axis=1)
df['Active'] = df['score'] >= 0.5
df.to_csv(os.path.join(folder, 'composed.csv'))

if len(argv) > 1:
    n = int(argv[1])
else:
    n = 57

df['score'] = df[scores].mean(axis=1)
df['Active'] = df['score'] >= 0.5
dft = df.sort_values('score', ascending=False)
dft.iloc[n:, list(df.columns).index('Active')] = False
dft = dft.loc[df.index]
dft.to_csv('ensemble_test_results.csv')