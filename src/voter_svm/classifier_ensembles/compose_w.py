import os
import pandas as pd
import numpy as np
from sys import argv


folder = 'Results_w'
models = 'Saved_models_w'
df_test = 'Data/test.csv'

filenames = list(filter(lambda x: x.endswith('.csv'), os.listdir(folder)))
filenames = list(map(lambda x: os.path.join(folder, x), filenames))

filenames_m = list(filter(lambda x: x.endswith('.model'), os.listdir(models)))
fscores = dict()
sm = 0
for filename in filenames_m:
    filename = filename.split('.model')[0]
    its = filename.split('.')
    fscores[int(its[0])] = float(f'0.{its[-1]}')
    sm += fscores[int(its[0])]

df = pd.read_csv(df_test, index_col=0)
scores = list()
fs = list()

for filename in filenames:
    dft = pd.read_csv(filename, index_col=0)
    try:
        n = int(filename.split('/')[-1].split('.csv')[0])
    except ValueError:
        continue
    f = fscores[n]
    df[f'score{n}'] = dft.score * f 
    scores.append(f'score{n}')
df['score'] = df[scores].sum(axis=1) / sm
df['Active'] = df['score'] >= 0.5
df.to_csv(os.path.join(folder, 'composed.csv'))

if len(argv) > 1:
    n = int(argv[1])
else:
    n = 57

df['score'] = df[scores].sum(axis=1) / sm
df['Active'] = df['score'] >= 0.5
dft = df.sort_values('score', ascending=False)
dft.iloc[n:, list(df.columns).index('Active')] = False
dft = dft.loc[df.index]
dft.to_csv(os.path.join(folder, 'composed_57.csv'))