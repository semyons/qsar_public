from torch.nn import BCEWithLogitsLoss
from dgllife.model.model_zoo.gcn_predictor import GCNPredictor
from data import MoleculeDataset
from torch.utils.data import DataLoader
from torch import nn
from sklearn.metrics import f1_score
import torch
import pandas as pd
import numpy as np
import random
import dgl
import os


use_cuda = False
force_reload = False
cutoff = False
data_folder = 'Data'
res_folder = 'Results'
mod_folder = 'Saved_models'

os.makedirs(res_folder, exist_ok=True)

class GCNWrapper(GCNPredictor):
    def forward(self, g):
        if use_cuda:
            g = g.to('cuda')
        nodes = g.ndata['h']
        return super().forward(g, nodes)


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    dgl.seed(seed)

def save_model(filename, model, swa_model, optimizer, scheduler, swa, seed):
    d = {'model': model.state_dict(),
         'swa_model': swa_model.state_dict(),
         'optimizer': optimizer.state_dict(),
         'scheduler': scheduler.state_dict(), 'swa': swa.state_dict(),
         'seed': seed}
    torch.save(d, os.path.join(mod_folder, filename))

def predict(model, dataset):
    res = model.forward(dgl.batch(dataset.graphs))
    p = torch.sigmoid(res).cpu().detach().numpy()
    return p, p >= 0.5

def calc_score(model, dataset):
    _, inds = predict(model, dataset)
    return f1_score(dataset.labels, inds)
    

set_seed(hash('#SaveSemyons') % (2147483647 - 1))


print('Loading datasets...')
dataset_train = MoleculeDataset('Data/train.csv', force_reload=force_reload, 
                                features='cannon',
                                cutoff=None,
                                triplets=False)
dataset_test = MoleculeDataset('Data/test.csv', force_reload=force_reload, 
                                features='cannon',
                                cutoff=None,
                                triplets=False)
n_nodes = dataset_train.graphs[0].node_attr_schemes()['h'].shape[0]
print('Evaluating models...')
model = GCNWrapper(n_nodes, hidden_feats=[32, 32], 
                    predictor_hidden_feats=64)
swa_model = torch.optim.swa_utils.AveragedModel(model)

df_train = pd.read_csv('Data/train.csv', index_col=0)
df_test = pd.read_csv('Data/test.csv', index_col=0)
dft = df_test.copy()
ids_train = [g.mol_id for g in dataset_train.graphs]
ids_test = [g.mol_id for g in dataset_test.graphs]
for file in os.listdir(mod_folder):
    if not file.endswith('.model'):
        continue
    try:
        n = int(file.split('.')[0])
    except ValueError:
        continue
    file = os.path.join(mod_folder, file)
    swa_model.load_state_dict(torch.load(file)['swa_model'])
    preds = predict(swa_model, dataset_test)[0]
    df_test.loc[ids_test, f'score{n}'] = 0.0
    df_test.loc[ids_test, f'score{n}'] = preds
    dft = dft.copy()
    dft['score'] = 0.0; dft.loc[ids_test, 'score'] = preds
    preds = predict(swa_model, dataset_train)[0]
    df_train.loc[ids_train, f'score{n}'] = 0.0
    df_train.loc[ids_train, f'score{n}'] = preds
    dft.to_csv(os.path.join(res_folder, f'{n}.csv'))

df_train.to_csv('ensemble_train.csv')
df_test.to_csv('ensemble_test.csv')