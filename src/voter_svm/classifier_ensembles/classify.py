from torch.nn import BCEWithLogitsLoss
from dgllife.model.model_zoo.gcn_predictor import GCNPredictor
from data import MoleculeDataset
from torch.utils.data import DataLoader
from torch import nn
from sklearn.metrics import f1_score
import torch
import pandas as pd
import numpy as np
import random
import dgl
import os
from sys import argv

mod_a = int(argv[1])
mod_b = int(argv[2])
know_false = [122, 1495, 401, 429, 437, 352, 1125,
              1332, 926, 796, 179, 13, 1595, 1596, 77]
know_true = [643, 1487, 1419, 190]
if len(argv) > 3:
    use_cuda = bool(argv[3])
else:
    use_cuda = True
force_reload = False
cutoff = False
lr = 3e-4
lr_swa = 1e-2
patience = 30
num_adam_epochs = 400
num_sw_epochs = 100
n_msg = 100
weighting = 5.0
batch_size = 32
data_folder = 'Data'
res_folder = 'Results'
mod_folder = 'Saved_models'
feats = 'cannon'
os.makedirs(res_folder, exist_ok=True)
os.makedirs(mod_folder, exist_ok=True)


class GCNWrapper(GCNPredictor):
    def forward(self, g):
        if use_cuda:
            g = g.to('cuda')
        nodes = g.ndata['h']
        return super().forward(g, nodes)


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    dgl.seed(seed)

def save_model(filename, model, swa_model, optimizer, scheduler, swa, seed):
    d = {'model': model.state_dict(),
         'swa_model': swa_model.state_dict(),
         'optimizer': optimizer.state_dict(),
         'scheduler': scheduler.state_dict(), 'swa': swa.state_dict(),
         'seed': seed}
    torch.save(d, os.path.join(mod_folder, filename))

def predict(model, dataset):
    res = model.forward(dgl.batch(dataset.graphs))
    p = torch.sigmoid(res).cpu().detach().numpy()
    return p, p >= 0.5

def calc_score(model, dataset):
    _, inds = predict(model, dataset)
    return f1_score(dataset.labels, inds)
    

set_seed(hash('#SaveSemyons') % (2147483647 - 1))


dataset_train = MoleculeDataset('Data/train.csv', force_reload=force_reload, 
                                features=feats,
                                cutoff=None,
                                triplets=False)
dataset_test = MoleculeDataset('Data/test.csv', force_reload=force_reload,
                               cutoff=None,
                               features=feats,
                               triplets=False)
df_test = pd.read_csv('Data/test.csv', index_col=0)
weights = [weighting if label else 1.0 for label in dataset_train.labels]
sampler = torch.utils.data.WeightedRandomSampler(weights,
                                                 len(dataset_train.graphs))
dataloader_train = torch.utils.data.DataLoader(dataset_train,
                                               batch_size=batch_size,
                                               collate_fn=dataset_train.get_collate(),
                                               # shuffle=True,
                                                sampler=sampler
                                               )

for seed_add in range(mod_a, mod_b):
    print(f'Seed #{seed_add}.')
    seed = (hash('#SaveSemyons') + seed_add) % (2147483647 - 1) 
    
    set_seed(seed)
    df = pd.read_csv('Data/train.csv', index_col=0)
    
    n_nodes = dataset_train.graphs[0].node_attr_schemes()['h'].shape[0]
    
    model = GCNWrapper(n_nodes, hidden_feats=[32, 32], 
                       predictor_hidden_feats=64)
    if use_cuda:
        model = model.cuda()
    optimizer = torch.optim.AdamW(model.parameters(), lr=lr)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',
                                                           patience=patience)
    sw = torch.optim.swa_utils.SWALR(optimizer, lr_swa)
    swa_model = torch.optim.swa_utils.AveragedModel(model)
    loss_fn = BCEWithLogitsLoss()
    set_seed(seed)
    
    print('Running regular AdamW sequence...')
    lf_accum = 0
    for epoch in range(1, num_adam_epochs + 1):
        if not epoch % n_msg:
            with torch.no_grad():
                f1 = calc_score(model, dataset_train)
            lf_accum /= n_msg
            print(f'Epoch {epoch}. Loss: {lf_accum:.6f}, f1 [train]: {f1:.3f}')
            lf_accum = 0
        lf_accum_epoch = 0
        for g, labels in dataloader_train:
            if use_cuda:
                labels = labels.cuda()
            optimizer.zero_grad()
            loss = loss_fn(model.forward(g), labels)
            loss.backward()
            optimizer.step()
            with torch.no_grad():
                lf_accum_epoch += float(loss)
        scheduler.step(lf_accum_epoch)
        lf_accum += lf_accum_epoch
    
    pscores, inds = predict(model, dataset_test)
    ids = [g.mol_id for g in dataset_test.graphs]
    df_test.loc[ids, 'score'] = pscores
    df_test.loc[ids, 'Active'] = inds
    kacts = np.array(df_test.loc[know_true, 'Active'], dtype=bool)
    kfalse = np.array(df_test.loc[know_false, 'Active'], dtype=bool)
    print(f'True: {kacts.sum()}/{len(kacts)}, False: {(~kfalse).sum()}/{len(kfalse)}.')
    print('Running SWA..')
    for epoch in range(epoch + 1, epoch + num_sw_epochs + 1):
        if not epoch % n_msg:
            lf_accum /= n_msg
            print(f'Epoch {epoch}. Loss: {lf_accum:.6f} [SWA]')
            lf_accum = 0
        lf_accum_epoch = 0
        for g, labels in dataloader_train:
            if use_cuda:
                labels = labels.cuda()
            optimizer.zero_grad()
            loss = loss_fn(model.forward(g), labels)
            loss.backward()
            optimizer.step()
            with torch.no_grad():
                lf_accum_epoch += float(loss)
        swa_model.update_parameters(model)
        sw.step()
        lf_accum += lf_accum_epoch
    print('Done. Making predictions...')
    torch.optim.swa_utils.update_bn(dataloader_train, swa_model)
    pscores, inds = predict(swa_model, dataset_test)
    ids = [g.mol_id for g in dataset_test.graphs]
    df_test.loc[ids, 'score'] = pscores
    df_test.loc[ids, 'Active'] = inds
    df_test.to_csv(os.path.join(res_folder, f'{seed_add}.csv'))
    kacts = np.array(df_test.loc[know_true, 'Active'], dtype=bool)
    kfalse = np.array(df_test.loc[know_false, 'Active'], dtype=bool)
    print(f'True: {kacts.sum()}/{len(kacts)}, False: {(~kfalse).sum()}/{len(kfalse)}.')
    print('Done. Saving model...')
    save_model(f'{seed_add}.model', model, swa_model, optimizer, scheduler, sw,
               seed)
