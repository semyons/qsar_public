echo "Training models (will take a long time)."
echo "Classifier ensemble..."
cd classifier_ensembles
python3 classify.py
echo "Repulsion model..."
cd repulsion
python3 train_repulsion.py