# from sklearnex import patch_sklearn
# patch_sklearn()
# from model import AttentiveNet
from dgllife.model.model_zoo.gcn_predictor import GCNPredictor
from data import MoleculeDataset, _collate_fn
from torch.utils.data import DataLoader
from torch.nn import TripletMarginLoss, MSELoss
from torch import nn
from dgllife.model.model_zoo.schnet_predictor import SchNetPredictor
import torch
from dgllife.model.model_zoo.mgcn_predictor import MGCNPredictor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# from pytorch_metric_learning import losses, samplers, miners
from dgllife.model.model_zoo.gin_predictor import GINPredictor
from sklearn.model_selection import RepeatedKFold
import dgl
import random
import gc
import os
# from dimenet import DimeNet

seed = hash('Semyons') % (2147483647 - 1)
apply_reg = False
force_reload = False
use_cuda = False
featurizer = 'cannon'
num_dim = 2
n_train_batches = 32

model_name = 'GCN_default'
if model_name.startswith('MCGN'):
    featurizer = 'mcgn'
postfix = '_reg' if apply_reg else str()
postfix += '_cannon' if featurizer == 'cannon' else str()
    
if num_dim > 2:
    postfix += '_d' + str(num_dim)
fig_folder = f'figs_rep_{model_name}{postfix}'
model_folder = f'saved_models_rep_{model_name}{postfix}'
fig_filename = fig_folder + '/{}.png'
os.makedirs(fig_folder, exist_ok=True)
os.makedirs(model_folder, exist_ok=True)

random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
dgl.seed(seed)
if force_reload:
    df = pd.read_csv('Data/train.csv', index_col=0)
    df_val = df.sample(frac=0.1)
    df_train = df.drop(df_val.index)
    df_train.to_csv('Data/train_train.csv', sep=',')
    df_val.to_csv('Data/train_val.csv', sep=',')
    print(df_val['Active'].sum())
    del df
    del df_train
    del df_val

if model_name.startswith('GCN'):
    cutoff = None
else:
    cutoff = 5.0
dataset_train = MoleculeDataset('Data/train_train.csv', force_reload=force_reload, cutoff=cutoff,
                                triplets=False,
                                feat=featurizer)
dataset_val = MoleculeDataset('Data/train_val.csv', force_reload=force_reload, cutoff=cutoff,
                              triplets=False, feat=featurizer)
print(dataset_val.labels.sum())
dataset_test = MoleculeDataset('Data/test.csv', force_reload=force_reload, triplets=False, cutoff=cutoff,
                               feat=featurizer)
dl_test = DataLoader(dataset_test, batch_size=128, shuffle=False,
                     collate_fn=dataset_test.get_collate())
n_nodes = dataset_train.graphs[0].node_attr_schemes()['h'].shape[0]
if model_name.startswith('MCGN'):
    n_edges = dataset_train.graphs[0].edge_attr_schemes()['dist'].shape[0]
else:
    n_edges = dataset_train.graphs[0].edge_attr_schemes()['e'].shape[0]
# True
# dl_test = DataLoader(dataset_test, batch_size=len(dataset_test), collate_fn=dataset_test.get_collate())
# dl_val = DataLoader(dataset_val, batch_size=len(dataset_val), collate_fn=dataset_val.get_collate())
# model = AttentiveNet(n_nodes, n_edges, dropout=0.2, hidden_size=(4,),
                      # graph_feat_size=32, num_gcn_layers=2, 
                      # )#.to('cuda')
# model = SchNetPredictor(n_nodes)
# model = model.to('cuda')
if model_name == 'MCGN_small_3_layers':
    model = MGCNPredictor(feats=24, n_tasks=num_dim, num_node_types=60, n_layers=3,
                          predictor_hidden_feats=32)
elif model_name == 'MCGN_tiny_3_layers':
    model = MGCNPredictor(feats=10, n_tasks=num_dim, num_node_types=60, n_layers=3,
                          predictor_hidden_feats=8)
elif model_name == 'MCGN_small_2_layers':
    model = MGCNPredictor(feats=24, n_tasks=num_dim, num_node_types=60,
                          predictor_hidden_feats=32)
elif model_name == 'MCGN_default':
    model = MGCNPredictor(n_tasks=num_dim)
elif model_name == 'MCGN_medium':
    model = MGCNPredictor(feats=64, n_tasks=num_dim, num_node_types=60,
                          predictor_hidden_feats=32)
elif model_name == 'MCGN_medium_2_layer':
    model = MGCNPredictor(feats=64, n_tasks=num_dim, num_node_types=60,
                          predictor_hidden_feats=32, n_layers=2)
elif model_name == 'MCGN_small':
    model = MGCNPredictor(feats=32, n_tasks=num_dim, num_node_types=60,
                          predictor_hidden_feats=16)
elif model_name == 'GCN_1_layer':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[64,],)
elif model_name == 'GCN_small':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[24, 24],
                          predictor_hidden_feats=16, #dropout=[0.3, 0.3]
                          )
elif model_name == 'GCN_small_3_layers':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[24, 24, 24],
                          predictor_hidden_feats=16, #dropout=[0.3, 0.3]
                          )
elif model_name == 'GCN_medium':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[48, 48],
                          predictor_hidden_feats=32, #dropout=[0.3, 0.3]
                          )
elif model_name == 'GCN_3_layers':
    model = GCNPredictor(n_nodes, n_tasks=num_dim,
                         hidden_feats=[64, 64, 64])
elif model_name == 'GCN_4_layers':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[48, 48, 48, 48],
                          predictor_hidden_feats=32, dropout=[0.4, 0.4, 0.4, 0.3]
                          )
elif model_name == 'GCN_default':
    model = GCNPredictor(n_nodes, n_tasks=num_dim)
elif model_name == 'GCN_smaller':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, 
                         hidden_feats=[58, 58],
                         predictor_hidden_feats=96)
elif model_name == 'GCN_bigger':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[96, 76])
elif model_name == 'GCN_big':
    model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[128, 96],
                         predictor_hidden_feats=256)
    
if use_cuda:
    model = model.to('cuda')
# with torch.no_grad():
#     model = DimeNet()

def forward_model(g, model, l_g=None):
    # g = g.to('cuda')
    if use_cuda:
        g = g.to('cuda')
    if model_name.startswith('MCGN'):
        edges = g.edata['dist']
        nodes = g.ndata['h'].to(torch.int64).flatten()
        return model.forward(g, nodes, edges)
    nodes = g.ndata['h']
    return model.forward(g, nodes)

def embedding(model, data):
    model.eval()
    g = dgl.batch(data.graphs)
    r = forward_model(g, model).cpu().detach().numpy()
    model.train()
    return np.array(r).reshape(-1, num_dim)

global_val_counts = None
global_test_counts = None

def plot(train, val, test, label_train, label_val, filename=None, ret_est=False):
    # gr = GridSearchCV(KNeighborsClassifier(),
    #                   param_grid={'n_neighbors': list(range(1, 4)),
    #                               'weights': ['uniform', 'distance']},
    #                   scoring='f1',
    #                   cv=RepeatedKFold(random_state=seed, n_splits=10)).fit(train, label_train)
    # train_res = gr.best_score_
    # # est = gr.best_estimator_.fit(train, label_train)
    # est = KNeighborsClassifier(1).fit(train, label_train)
    # preds = est.predict(val)
    norms_val = np.linalg.norm(val, axis=1)
    # norms_val /= norms_val.mean()
    best_r = -float('inf')
    res = -1
    for rad in np.linspace(0, 10, 1000):
        r = f1_score(norms_val > rad, label_val)
        if r > res:
            res = r
            best_r = rad
    preds = norms_val > rad
    # f = label_val != preds
    # fp = preds & f
    # fn = (~preds) & f
    train_res = f1_score(np.linalg.norm(train, axis=1) > best_r, label_train)
    # res = f1_score(label_val, preds)
    if num_dim == 2:
        ps = np.append(train, val, axis=0)
        xmin, ymin = ps.min(axis=0) - 1
        xmax, ymax = ps.max(axis=0) + 1
        xm, ym = ps.mean(axis=0)
        xs, ys = ps.std(axis=0) * 6
        xmin = max(xmin, xm - xs); xmax = min(xmax, xm + xs)
        ymin = max(ymin, ym - ys); ymax = min(ymax, ym + ys)
        # xmin = min(train[:,0].min(), val[:, 0].min()) - 0.5
        # xmax = max(train[:,0].max(), val[:, 0].max()) + 0.5
        # xmin = max(-15, xmin); xmax = min(15, xmax)
        # ymin = min(train[:,1].min(), val[:, 1].min()) - 0.5
        # ymax = max(train[:,1].max(), val[:, 1].max()) + 0.5
        # ymin = max(-15, ymin); ymax = min(15, ymax)
        plt.figure(dpi=200, figsize=(21, 6))
        ax = plt.subplot(1, 3, 1)
        plt.scatter(train[:, 0], train[:, 1], s=0.75)
        plt.scatter(train[:, 0][label_train], train[:, 1][label_train], s=0.75, c='r')
        ax.add_patch(plt.Circle([0.0, 0.0], radius=best_r, color='r', fill=False))
        # plt.xlim(xmin, xmax)
        # plt.ylim(ymin, ymax)
        plt.title('Training set ({:.3f})'.format(train_res))
        ax = plt.subplot(1, 3, 2)
        plt.scatter(val[:, 0][~label_val], val[:, 1][~label_val], s=0.75)
        plt.scatter(val[:, 0][label_val], val[:, 1][label_val], s=0.75, c='r')
        # f = val[fn]
        # plt.scatter(f[:, 0], f[:, 1], s=1, c='g', marker='x')
        # f = val[fp]
        # plt.scatter(f[:, 0], f[:, 1], s=1, c='k', marker='x')
        ax.add_patch(plt.Circle([0.0, 0.0], radius=best_r, color='r', fill=False))
        # plt.xlim(xmin, xmax)
        # plt.ylim(ymin, ymax)
        plt.yticks([])
        plt.title('Validation set ({:.3f}, r={:.2f})'.format(res, best_r))
        plt.tight_layout()
        ax = plt.subplot(1, 3, 3)
        plt.scatter(test[:, 0], test[:, 1], s=0.75, c='k')
        ax.add_patch(plt.Circle([0.0, 0.0], radius=best_r, color='r', fill=False))
        # plt.xlim(xmin, xmax)
        # plt.ylim(ymin, ymax)
        plt.yticks([])
        plt.title('Test')
        plt.tight_layout()
        if filename:
            plt.savefig(filename)
        try:
            plt.close()
        except Exception:
            pass
    elif num_dim == 3:
        ps = np.append(train, val, axis=0)
        xmin, ymin, zmin = ps.min(axis=0) - 1
        xmax, ymax, zmax = ps.max(axis=0) + 1
        xm, ym, zm = ps.mean(axis=0)
        xs, ys, zs = ps.std(axis=0) * 6
        xmin = max(xmin, xm - xs); xmax = min(xmax, xm + xs); zmax = min(zmax, zm + zs)
        ymin = max(ymin, ym - ys); ymax = min(ymax, ym + ys); ymax = min(zmax, zm + zs)
        fig = plt.figure(figsize=(14, 6), dpi=200)
        ax = fig.add_subplot(1, 2, 1, projection='3d')
        pos = train[label_train]
        neg = train[~label_train]
        ax.scatter(neg[:, 0], neg[:, 1], neg[:, 2], s=1.5, c='b')
        ax.scatter(pos[:, 0], pos[:, 1], pos[:, 2], s=1.5, c='r')
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        ax.set_zlim(zmin, zmax)
        plt.title('Training set ({:.3f})'.format(train_res))
        ax = fig.add_subplot(1, 2, 2, projection='3d')
        pos = val[label_val]
        neg = val[~label_val]
        val_fneg = val[(label_val != preds) & (~label_val)]
        val_fpos = val[(label_val != preds) & (label_val)]
        ax.scatter(neg[:, 0], neg[:, 1], neg[:, 2], s=1.5, c='b')
        ax.scatter(pos[:, 0], pos[:, 1], pos[:, 2], s=1.5, c='r')
        ax.scatter(val_fneg[:, 0], val_fneg[:, 1], val_fneg[:, 2], s=1, c='g', marker='x')
        ax.scatter(val_fpos[:, 0], val_fpos[:, 1], val_fpos[:, 2], s=1, c='k', marker='x')
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        ax.set_zlim(zmin, zmax)
        plt.title('Validation set ({:.3f})'.format(res))
        plt.tight_layout()
        if filename:
            plt.savefig(filename)
        try:
            plt.close()
        except Exception:
            pass
    else:
        pass
    if ret_est:
        return res, train_res
    return res, train_res
import os

with torch.no_grad():
    train_emb = embedding(model, dataset_train)
    val_emb = embedding(model, dataset_val)
    test_emb = embedding(model, dataset_test)
plot(train_emb, val_emb, test_emb, dataset_train.labels, dataset_val.labels, fig_filename.format(0))

def tr_loss(a, p, n, margin=10, b=0.01):
    dp = torch.sum((a - p) ** 2, axis=1) ** 0.5
    dpn = torch.sum((a - n) ** 2, axis=1) ** 0.5
    # print(a)
    # print(dp)
    return torch.max(dp - dpn + margin, torch.tensor(0.0)).mean()
    

# def update_global_counts(model, knn):
#     with torch.no_grad():
#         emb = embedding(model, dataset_train)
        
        
    
    

triplet_loss = TripletMarginLoss(margin=0.5)
alpha = 0.01
# lf = tr_loss

def rep_loss(embs, labels, reduce=True):
    norms = torch.norm(embs, dim=1)
    norms /= norms.mean()
    res = torch.zeros_like(norms)
    if use_cuda:
        labels = labels.cuda()
    labs_pos = labels & (norms < 10)
    res[labs_pos] = -40.0 * norms[labs_pos] * torch.exp(-2 * (norms[labs_pos] + 2.0))
    res[~labels] = norms[~labels] * torch.exp(2 * (norms[~labels] - 0.1))
    return res.mean() if reduce else res

def update_weights(embs, labels, inds, weights):
    dists = torch.abs(rep_loss(embs, labels))
    dists /= dists.sum()
    weights[inds] = dists.cpu().detach().numpy()

def dist_shift_loss(embs, test_embs):
    mean = embs.mean(dim=0)
    mean_embs = test_embs.mean(dim=0)
    std = embs.std(dim=0)
    std_test = test_embs.std(dim=0)
    return torch.norm(mean - mean_embs) + torch.norm(std - std_test)

def save_model(file: str, loss=None):
    if loss is not None:
        with open(os.path.join(model_folder, file + '.txt'), 'w') as f:
            train, test = loss
            f.write('Train: {:.3f}, Test: {:.3f}'.format(train, test))
    d = {'model': model.state_dict(), 'optimizer': optimizer.state_dict()}
    torch.save(d, os.path.join(model_folder, file))


random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
dgl.seed(seed)

optimizer = torch.optim.AdamW(model.parameters(), lr=0.0001)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min',
                                                       patience=4)

best_res = -float('inf')



epochs = 20000
epoch = 1
msg_report = 25
sampler = None
weights = np.zeros(len(dataset_train.graphs))
try:
    while epoch < epochs:
        lf_accum = 0
        lf_reg = 0
        dl_train = DataLoader(dataset_train, batch_size=n_train_batches,
                              collate_fn=dataset_train.get_collate(),
                              sampler=sampler
                              )
        for i, gs, _, labels in dl_train:
            labels = labels.bool()
            embs = forward_model(gs, model)
            if apply_reg:
                test_embs = forward_model(next(iter(dl_test))[1], model)
                reg = dist_shift_loss(embs, test_embs)
            else:
                reg = 0.0
            loss = rep_loss(embs, labels) + reg
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            with torch.no_grad():
                lf_accum += loss
                lf_reg += reg
                update_weights(embs, labels, i, weights)
        sampler = torch.utils.data.WeightedRandomSampler(weights,
                                                         len(dataset_train.graphs),
                                                         replacement=True)
        if not epoch % msg_report:
            print('Epoch', epoch)
            with torch.no_grad():
                scheduler.step(lf_accum)
                if epoch % 100 == 0:
                    save_model(f'{epoch}.model')
                loss = float(lf_accum) / msg_report * 100
                lf_reg = float(lf_reg) / msg_report * 100
                train_emb = embedding(model, dataset_train)
                val_emb = embedding(model, dataset_val)
                if apply_reg or num_dim == 2:
                    test_emb = embedding(model, dataset_test)
                else:
                    test_emb = None
                b, t = plot(train_emb, val_emb, test_emb, dataset_train.labels, dataset_val.labels, fig_filename.format(epoch))
                if b > best_res:
                    best_res = b
                    if b > 0.3:
                        save_model('{:.3f}.{}.model'.format(b, epoch))
                if apply_reg:
                    print("Loss: {:.6f}, without reg: {:.6f}".format(loss, loss - lf_reg))
                else:
                    print("Loss: {:.6f}".format(lf_accum))
                print('F1 = {:.3f}, F1 [train] = {:.3f}| F1 [best] = {:.3f}'.format(b, t, best_res))
                i = 0
                gc.collect()
        epoch += 1
except KeyboardInterrupt:
    save_model(f'last.{epoch}.model')
