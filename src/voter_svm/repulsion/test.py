# from sklearnex import patch_sklearn
# patch_sklearn()
from data import MoleculeDataset, _collate_fn
from dgllife.model.model_zoo.gcn_predictor import GCNPredictor
from torch.utils.data import DataLoader
from torch.nn import TripletMarginLoss, MSELoss
from torch import nn
from dgllife.model.model_zoo.schnet_predictor import SchNetPredictor
import torch
from dgllife.model.model_zoo.mgcn_predictor import MGCNPredictor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pytorch_metric_learning import losses, samplers, miners
from dgllife.model.model_zoo.gin_predictor import GINPredictor
from sklearn.model_selection import RepeatedKFold
import dgl
import random
import gc
import os

folder_results = 'results'
os.makedirs(folder_results, exist_ok=True)
filenames = [
             'saved_models_rep_GCN_default_cannon/0.560.1500.model',
             # 'saved_models_rep_GCN_default_reg_cannon/500.model',
            # 'saved_models_rep_GCN_default_reg_cannon/800.model',
             # 'saved_models_rep_GCN_default_reg_cannon/0.474.200.model'
             ]

for filename in filenames:
    checkpoint = torch.load(filename)
    seed = hash('Semyons') % (2147483647 - 1)
    force_reload = False
    use_cuda = False
    featurizer = 'cannon'
    num_dim = 2
    if filename.startswith('alt'):
        data_folder = 'Data_alt'
    else:
        data_folder = 'Data'
    
    model_name = '_'.join(filename.split('/')[0].split('_')[3:])
    
    if 'd3' in filename:
        num_dim = 3
    if '_reg' in model_name:
        model_name = model_name.split('_reg')[0]
    if '_cannon' in model_name:
        model_name = model_name.split('_cannon')[0]
    if model_name.startswith('MCGN'):
        featurizer = 'mcgn'
    featurizer = 'cannon'
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    dgl.seed(seed)
    print(model_name)
    if model_name.startswith('GCN'):
        cutoff = None
    else:
        cutoff = 5.0
    dataset_train = MoleculeDataset(f'{data_folder}/train_train.csv', force_reload=force_reload, cutoff=cutoff, cache_dir=data_folder,
                                    triplets=False,
                                    feat=featurizer)
    dataset_val = MoleculeDataset(f'{data_folder}/train_val.csv', force_reload=force_reload, cutoff=cutoff, cache_dir=data_folder,
                                  triplets=False, feat=featurizer)
    dataset_test = MoleculeDataset('Data/test.csv', force_reload=force_reload, triplets=False, cutoff=cutoff,
                                   feat=featurizer)
    n_nodes = dataset_train.graphs[0].node_attr_schemes()['h'].shape[0]
    if model_name.startswith('MCGN'):
        n_edges = dataset_train.graphs[0].edge_attr_schemes()['dist'].shape[0]
    else:
        n_edges = dataset_train.graphs[0].edge_attr_schemes()['e'].shape[0]
    # True
    # dl_train = DataLoader(dataset_train, batch_size=64, collate_fn=dataset_train.get_collate(),
    #                       )
    
    
    if model_name == 'MCGN_small_3_layers':
        model = MGCNPredictor(feats=24, n_tasks=num_dim, num_node_types=60, n_layers=3,
                              predictor_hidden_feats=32)
    elif model_name == 'MCGN_tiny_3_layers':
        model = MGCNPredictor(feats=10, n_tasks=num_dim, num_node_types=60, n_layers=3,
                              predictor_hidden_feats=8)
    elif model_name == 'MCGN_small_2_layers':
        model = MGCNPredictor(feats=24, n_tasks=num_dim, num_node_types=60,
                              predictor_hidden_feats=32)
    elif model_name == 'MCGN_default':
        model = MGCNPredictor(n_tasks=num_dim)
    elif model_name == 'MCGN_medium':
        model = MGCNPredictor(feats=64, n_tasks=num_dim, num_node_types=60,
                              predictor_hidden_feats=32)
    elif model_name == 'MCGN_medium_2_layer':
        model = MGCNPredictor(feats=64, n_tasks=num_dim, num_node_types=60,
                              predictor_hidden_feats=32, n_layers=2)
    elif model_name == 'MCGN_small':
        model = MGCNPredictor(feats=32, n_tasks=num_dim, num_node_types=60,
                              predictor_hidden_feats=16)
    elif model_name == 'GCN_small':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[24, 24],
                              predictor_hidden_feats=16, #dropout=[0.3, 0.3]
                              )
    elif model_name == 'GCN_small_3_layers':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[24, 24, 24],
                              predictor_hidden_feats=16, #dropout=[0.3, 0.3]
                              )
    elif model_name == 'GCN_medium':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[48, 48],
                              predictor_hidden_feats=32, #dropout=[0.3, 0.3]
                              )
    elif model_name == 'GCN_4_layers':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[48, 48, 48, 48],
                              predictor_hidden_feats=32, dropout=[0.4, 0.4, 0.4, 0.3]
                              )
    elif model_name == 'GCN_default':
        model = GCNPredictor(n_nodes, n_tasks=num_dim)
    elif model_name == 'GCN_3_layers':
        model = GCNPredictor(n_nodes, n_tasks=num_dim,
                             hidden_feats=[64, 64, 64])
    elif model_name == 'GCN_smaller':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, 
                             hidden_feats=[58, 58],
                             predictor_hidden_feats=96)
    elif model_name == 'GCN_bigger':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[96, 76])
    elif model_name == 'GCN_big':
        model = GCNPredictor(n_nodes, n_tasks=num_dim, hidden_feats=[128, 96],
                             predictor_hidden_feats=186)
    else:
        raise Exception("Unknown model")
        
    if use_cuda:
        model = model.to('cuda')
    model.load_state_dict(checkpoint['model'])
    
    def forward_model(g, model, l_g=None):
        # g = g.to('cuda')
        if use_cuda:
            g = g.to('cuda')
        if model_name.startswith('MCGN'):
            edges = g.edata['dist']
            nodes = g.ndata['h'].to(torch.int64).flatten()
            return model.forward(g, nodes, edges)
        nodes = g.ndata['h']
        return model.forward(g, nodes)
    
    def embedding(model, data):
        model.eval()
        r = list(map(lambda x: forward_model(x, model).cpu().detach().numpy(), data.graphs))
        model.train()
        return np.array(r).reshape(-1, num_dim)
    
    def calc_r(data, labels):
        norms = np.linalg.norm(data, axis=1)
        best_f1 = -1
        best_r = 0
        for r in np.linspace(0, 10, 1000):
            f1 = f1_score(labels, norms > r)
            if f1 > best_f1:
                best_f1 = f1
                best_r = r
        return best_r, best_f1
            
            
    
    emb_train = embedding(model, dataset_train)
    emb_val = embedding(model, dataset_val)
    
    r_train, ft = calc_r(emb_train, dataset_train.labels)
    r_val, f_val = calc_r(emb_val, dataset_val.labels)
    f_train = f1_score(dataset_val.labels, np.linalg.norm(emb_val, axis=1) > r_train)
    print(f'f_val = {f_val}, f_train = {f_train}, r_val = {r_val}, r_train = {r_train}')
    
    emb_test = embedding(model, dataset_test)
    norms = np.linalg.norm(emb_test, axis=1)
    ids = [m.mol_id for m in dataset_test.graphs]
    df = pd.read_csv('Data/test.csv', index_col=0)
    df.loc[ids, 'norm'] = norms
    df['Active'] = False
    postfixes = [str()]
    rs = [r_val]
    dfs = []
    filename = os.path.join(folder_results, f'{model_name}_{f_val:.2}')
    if not np.allclose(r_val, r_train):
        postfixes.append('_train')
        rs.append(r_train)
    for r, postfix in zip(rs, postfixes):
        df.loc[ids, 'Active'] = norms > r
        dfs.append(df.copy())
    if dfs and np.any(dfs[0].Active != dfs[1].Active):
        for df, p in zip(dfs, postfixes):
            df.to_csv(filename + p + '.csv', sep=',')
        r = (rs[0] + rs[1]) / 2
        df['Active'] = norms > r
        if np.any(dfs[0].Active != df.Active) or np.any(dfs[1].Active != df.Active):
            df.to_csv(filename + '_mid.csv', sep=',')
    else:
        df.to_csv(filename + '.csv', sep=',')
    plt.figure(dpi=200, figsize=(16,8))
    v = emb_train
    ax=plt.subplot(1,3,1);plt.scatter(v[:, 0], v[:,1], s=0.1, c='b')
    v = v[dataset_train.labels]
    plt.scatter(v[:, 0], v[:,1], s=0.1, c='r');
    ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
    plt.xlim(-5, 5); plt.ylim(-5, 5)
    plt.title('Train')
    v = emb_val
    ax=plt.subplot(1,3,2);plt.scatter(v[:, 0], v[:,1], s=0.1, c='b')
    v = v[dataset_val.labels]
    plt.scatter(v[:, 0], v[:,1], s=0.1, c='r');
    ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
    plt.xlim(-5, 5); plt.ylim(-5, 5)
    plt.title('Validation')
    v = emb_test
    ax=plt.subplot(1,3,3);plt.scatter(v[:, 0], v[:,1], s=0.1, c='k')
    # v = [norms > r]
    ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
    plt.xlim(-5, 5); plt.ylim(-5, 5)
    plt.title('Test')
    # break

# plt.figure(dpi=200, figsize=(16,8))
# v = emb_train
# ax=plt.subplot(1,3,1);plt.scatter(v[:, 0], v[:,1], s=0.1, c='b')
# v = v[dataset_train.labels]
# plt.scatter(v[:, 0], v[:,1], s=0.1, c='r');
# ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
# plt.xlim(-5, 5); plt.ylim(-5, 5)
# plt.title('Train')
# v = emb_val
# ax=plt.subplot(1,3,2);plt.scatter(v[:, 0], v[:,1], s=0.1, c='b')
# v = v[dataset_val.labels]
# plt.scatter(v[:, 0], v[:,1], s=0.1, c='r');
# ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
# plt.xlim(-5, 5); plt.ylim(-5, 5)
# plt.title('Validation')
# v = emb_test
# ax=plt.subplot(1,3,3);plt.scatter(v[:, 0], v[:,1], s=0.1, c='k')
# # v = [norms > r]
# ax.add_patch(plt.Circle([0,0],radius=rs[0], fill=False))
# plt.xlim(-5, 5); plt.ylim(-5, 5)
# plt.title('Test')