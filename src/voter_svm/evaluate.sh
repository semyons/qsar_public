echo "Running inference via trained models."
echo "Classifier ensemble..."
cd classifier_ensembles
python3 eval.py
python3 compose.py
echo "Repulsion model..."
cd repulsion
python3 test.py
