from pathlib import Path
from typing import Optional, List, Tuple

import deepchem as dc
import numpy as np
import pandas as pd
import git


def get_data_dir():
    repo = git.Repo(".", search_parent_directories=True)
    return Path(repo.working_tree_dir) / "data"


def get_out_dir():
    return get_data_dir() / "out"


def get_submissions_dir():
    return get_data_dir() / "submissions"


def read_table(stem: str, parent_dir: Optional[Path] = None, index_col: Optional[int] = 0) -> pd.DataFrame:
    return pd.read_csv((parent_dir if parent_dir else get_data_dir()) / f"{stem}.csv", index_col=index_col)


def load_data(extended: bool, prefix: str) -> pd.DataFrame:
    result = read_table(prefix)
    if extended:
        prefix = prefix.split("_")[0]
        result = result.join(read_table(f"{prefix}AnnaFeatures", index_col=None))
    return result


def load_train(extended: bool = False) -> pd.DataFrame:
    return load_data(extended=extended, prefix="train_extended")


def load_test(extended: bool = False) -> pd.DataFrame:
    return load_data(extended=extended, prefix="test")


class ColName:
    mol = "mol"
    hmol = "molWithH"
    smi = "Smiles"
    active = "Active"
    stratified_split = "StratifiedSplit"
    strat_6fold_split = "Strat6foldSplit"
    butina_split = "ButinaSplit"
    minmax_split = "MinMaxSplit"


def load_train_splits(extended: bool,
                      split_colname: str = ColName.stratified_split) -> List[Tuple[pd.DataFrame, pd.DataFrame]]:
    train = load_train(extended=extended)
    result = []
    for split_idx in np.unique(train[split_colname]):
        train_part = train[train[split_colname] != split_idx]
        valid_part = train[train[split_colname] == split_idx]
        result.append((train_part, valid_part))
    return result


def make_deepchem(data: pd.DataFrame, featurizer: dc.feat.Featurizer) -> dc.data.NumpyDataset:
    features = featurizer.featurize(data[ColName.smi])
    indices_to_keep = np.where([not isinstance(elem, np.ndarray) for elem in features])
    return dc.data.NumpyDataset(X=features[indices_to_keep],
                                y=np.array(data[ColName.active].iloc[indices_to_keep])
                                if ColName.active in data else None)
